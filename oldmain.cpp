#include <algorithm>
#include <vector>
#include <iostream>
#include <set>
#include <utility>

#include "util.h"

namespace romdo
{

using namespace std;
using namespace util;

typedef map<string, string> record;
typedef map<string, record> database;

const char * RECORD_KEY = "netid";
const char DELIMITOR = '\t';

record mergeRecords(const record & r1, const record & r2);
database mergeDatabases(const database & db1, const database & db2);
database readDatabase(const string & file);
void writeDatabase(database & db, const string & filename);
void writeHeader(ostream & out, const record & rec);
void writeRecord(ostream & out, const record & rec);
void alignAllDatabaseRecords(database & db);

int main(int argc, const char * const * argv)
{
	OptionsParser options;
	string databasefile = "";
	options.addArg(databasefile);
	vector<string> mergefiles = options.parse(argc, argv);
	
	database mainDatabase = readDatabase(databasefile);

	for (size_t mergefile_i = 0; mergefile_i < mergefiles.size(); mergefile_i++)
	{
		database mergeDatabase = readDatabase(mergefiles[mergefile_i]);
		mainDatabase = mergeDatabases(mainDatabase, mergeDatabase);
	}

	writeDatabase(mainDatabase, databasefile);

	return 0;
}

database readDatabase(const string & file)
{
	vector<string> lines;
	util::readFile(file, lines);

	cout << "reading database with " << lines.size() << " lines" << endl;

	for (size_t line_i = 0; line_i < lines.size(); line_i++)
		makeLower(lines[line_i]);

	database resultDatabase;
	if (lines.empty()) return resultDatabase;
	vector<string> headers = tokenize(lines[0], DELIMITOR);
	if (headers.back() == "") headers.pop_back();

	for (size_t line_i = 1; line_i < lines.size(); line_i++)
	{
		vector<string> columns = tokenize(lines[line_i], DELIMITOR);
		if (columns.back() == "") columns.pop_back();
		
		record currRecord;
		if (columns.size() > headers.size())
		{
			cerr << "Error: " << file << ":" << line_i << " has more columns than there are headers for this file:" << endl;
			cerr << lines[line_i] << endl;
		}

		for (size_t column_i = 0; column_i < headers.size(); column_i++)
			if (column_i < columns.size())
				currRecord[headers[column_i]] = columns[column_i];
			else
				currRecord[headers[column_i]] = "";
		
		resultDatabase[currRecord[RECORD_KEY]] = currRecord;
	}

	cout << "read database size " << resultDatabase.size() << endl;
	alignAllDatabaseRecords(resultDatabase);
	cout << "aligned read database size " << resultDatabase.size() << endl;
	return resultDatabase;
}

void writeDatabase(database & db, const string & filename)
{
	cout << "writing db size " << db.size() << endl;
	alignAllDatabaseRecords(db);
	ofstream outfile(filename.c_str(), ios_base::out | ios_base::trunc);
	
	if (!db.empty())
	{
		writeHeader(outfile, db.begin()->second);
	
		for (database::const_iterator it = db.begin(); it != db.end(); ++it)
			writeRecord(outfile, it->second);
	}
	
	outfile.close();
}

void writeHeader(ostream & out, const record & rec)
{
	out << RECORD_KEY;

	for (record::const_iterator it = rec.begin(); it != rec.end(); ++it)
		if (it->first != RECORD_KEY)
			out << DELIMITOR << it->first;
	
	out << endl;
}

void writeRecord(ostream & out, const record & rec)
{
	out << rec.find(RECORD_KEY)->second;

	for (record::const_iterator it = rec.begin(); it != rec.end(); ++it)
		if (it->first != RECORD_KEY)
			out << DELIMITOR << it->second;
	
	out << endl;
}

database mergeDatabases(const database & db1, const database & db2)
{
	cout << "merging db sizes " << db1.size() << " and " << db2.size() << endl;
	database result;

	for (database::const_iterator db1it = db1.begin(); db1it != db1.end(); ++db1it)
	{
		const database::const_iterator db2it = db2.find(db1it->first);
		
		record resultRecord;
		if (db2it == db2.end())
			resultRecord = db1it->second;
		else
			resultRecord = mergeRecords(db1it->second, db2it->second);
		
		result[resultRecord[RECORD_KEY]] = resultRecord;
	}
	
	for (database::const_iterator db2it = db2.begin(); db2it != db2.end(); ++db2it)
	{
		const database::const_iterator db1it = db1.find(db2it->first);
		
		record resultRecord;
		if (db1it == db1.end())
			resultRecord = db2it->second;
		else
			resultRecord = mergeRecords(db1it->second, db2it->second);
		
		result[resultRecord[RECORD_KEY]] = resultRecord;
	}
	
	cout << "merge base successful, size " << result.size() << endl;
	alignAllDatabaseRecords(result);
	cout << "merge alignment successful, size " << result.size() << endl;

	return result;
}

void alignAllDatabaseRecords(database & db)
{
	record recWithAllHeaders;
	for (database::const_iterator it = db.begin(); it != db.end(); ++it)
		recWithAllHeaders = mergeRecords(recWithAllHeaders, it->second);

	for (database::iterator dbit = db.begin(); dbit != db.end(); ++dbit)
	{
		for (record::iterator recit = recWithAllHeaders.begin(); recit != recWithAllHeaders.end(); ++recit)
		{
			if (dbit->second.find(recit->first) == dbit->second.end()) // not found
				dbit->second.insert(make_pair(recit->first, ""));
		}
	}
}

record mergeRecords(const record & r1, const record & r2)
{
	record result = r1;
	
	for (record::const_iterator r2it = r2.begin(); r2it != r2.end(); ++r2it)
		result[r2it->first] = r2it->second;
	
	return result;
}

} // namespace romdo

int main(int argc, char ** argv)
{
	romdo::main(argc, argv);
}

