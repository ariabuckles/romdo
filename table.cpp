#include "table.h"

// The column delimeter for the text representation
const char Table::delimiter = '\t';

// An empty record for help with get() const
const Record Table::emptyRecord;

vector<string> Table::keys() const
{
	vector<string> result;
	for (RecordMap::const_iterator it = records.begin(); it != records.end(); ++it)
		result.push_back(it->first);
	return result;
}

// Gets a const record from the map
const Record & Table::get(const string & key) const
{
	RecordMap::const_iterator it = records.find(key);
	if (it == records.end())
		return emptyRecord;
	else
		return it->second;
}

// Writes the header line to a stream
void Table::writeHeader(ostream & out)
{
	out << key_;

	for (HeaderSet::iterator it = headers.begin(); it != headers.end(); ++it)
		out << delimiter << *it;

	out << endl;
}

// Writes an individual record (one line of the table's text representation)
// to a stream
void Table::writeRecord(ostream & out, const Record & rec, const string & emptyValue)
{
	out << rec.key();

	for (HeaderSet::iterator it = headers.begin(); it != headers.end(); ++it)
	{
		out << delimiter;
		string value = rec[*it];
		if (value == Record::emptyValue)
			value = emptyValue;
		out << value;
	}
	
	out << endl;
}

// Writes this table's text representation to a stream
void Table::writeToStream(ostream & out, const string & emptyValue)
{
	writeHeader(out);

	for (RecordMap::const_iterator it = records.begin(); it != records.end(); ++it)
		writeRecord(out, it->second, emptyValue);
}

Table Table::evaluate() const
{
	Table result;
	result.key_ = key_;
	for (HeaderSet::iterator it = headers.begin(); it != headers.end(); ++it)
		result.addHeader(*it);
	for (RecordMap::const_iterator it = records.begin(); it != records.end(); ++it)
		result.insert(it->second.evaluate());
	return result;
}

// Read a Record object from a line, given the headers for that line
Record Table::parseRecord(const string & line, const vector<string> headervec)
{
	vector<string> columns = util::tokenize(line, delimiter);
	Record rec;
	if (columns.size() != 0)
		rec.key(columns[0]);
	
	if (columns.size() > headervec.size())
		throw util::Exception("Record has too many columns: " + rec.key());

	for (size_t i = 1; i < headervec.size() && i < columns.size(); i++)
			rec[headervec[i]] = columns[i];

	return rec;
}

// Insert/merge the record into this table
void Table::insert(const Record & rec)
{
	records[rec.key()].mergeValuesFrom(rec);
}

// Create a header for this table
void Table::addHeader(const string & header)
{
	headers.insert(header);
}

void Table::removeHeader(const string & header)
{
	headers.erase(header);
	for (RecordMap::iterator it = records.begin(); it != records.end(); ++it)
		it->second.erase(header);
}

// Combine the headers from a different HeaderSet
void Table::mergeHeaders(const HeaderSet & otherHeaders)
{
	for (HeaderSet::iterator otherHeader = otherHeaders.begin();
			otherHeader != otherHeaders.end();
			++otherHeader)
		addHeader(*otherHeader);
}

// Merge a second table into this table
// Any values present (not "" or emptyValue) in the second table will
// overwrite those values in this table
void Table::mergeRecordsFrom(const Table & otherDb)
{
	if (empty())
		key_ = otherDb.key_;
	else if (key_ != otherDb.key_)
		throw util::Exception("Error in merging headers: mismatching key_s: " + key_ + ", " + otherDb.key_);
	
	mergeHeaders(otherDb.headers);

	for (RecordMap::const_iterator it = otherDb.records.begin(); it != otherDb.records.end(); ++it)
		insert(it->second);
}

// Clear all contents of this table
void Table::clear()
{
	key_ = "";
	headers.clear();
	records.clear();
}

// Read a table from an input stream
void Table::readFromStream(istream & in)
{
	clear();
	string line;
	if (!getline(in, line))
		return;
	util::makeLower(line);
	util::replaceAll(line, "\r", "");
	
	vector<string> headervec = util::tokenize(line, delimiter);
	if (headervec.empty())
		throw util::Exception("Could not read Table; headers were empty");
	
	key_ = headervec[0];
	headers.insert(++headervec.begin(), headervec.end());

	while (getline(in, line))
	{
		util::makeLower(line);
		util::replaceAll(line, "\r", "");
		Record rec = parseRecord(line, headervec);
		insert(rec);
	}
}

// Read a table from a file
void Table::readFromFile(const string & filename)
{
	ifstream file(filename.c_str(), ios_base::in);
	readFromStream(file);
	file.close();
}

// Write this table's text representation to a file
void Table::writeToFile(const string & filename, const string & emptyValue)
{
	ofstream file(filename.c_str(), ios_base::out | ios_base::trunc);
	writeToStream(file, emptyValue);
	file.close();
}

