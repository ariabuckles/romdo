#include <algorithm>
#include <vector>
#include <iostream>
#include <set>
#include <utility>

#include "record.h"
#include "table.h"
#include "util.h"

using namespace std;
using namespace util;

/**
 * Romdo: A tool to manipulate text-based tables
 */
class Romdo
{
	public:
	// Construct an instance from a table
	explicit Romdo(const string & mainTableFilename) : mainTable(mainTableFilename) { }
	explicit Romdo(const Table & _mainTable) : mainTable(_mainTable) { }

	// Interact with this object's main table
	const Table & table() const { return mainTable; }
	void save(const string & mainTableFilename) { mainTable.writeToFile(mainTableFilename); }
	void exportto(const string & exportFilename) const { mainTable.evaluate().writeToFile(exportFilename, ""); }

	// Merge tables into this instance's main table
	void merge(const vector<Table> & tables);
	void merge(const vector<string> & files);

	// Show a specific record
	void show(const string & key, const string & column = "") const;
	void showall(const string & column) const;

	// Update a specific record
	void update(const string & key, const string & column, const string & value);
	void updateall(const string & column, const string & value);
	void deletecolumn(const string & column);

	// Display a histogram
	void histogram(const string & column);

	// List all record ids
	void list(const vector<string> & params) const;
	void list() const;

	private:
	Table mainTable;
};

