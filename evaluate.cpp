#include "evaluate.h"

map<string, string> evaluate(const map<string, string> & fields)
{
	lua_State * luaState = luaL_newstate();
	luaL_openlibs(luaState);
	map<string, string> results;

	int error_col = luaL_dostring(luaState, "columns = {}");
	if (error_col != LUA_OK) throw util::Exception(string("Lua Parse Error (columns): ") + lua_tostring(luaState, -1));

	for (map<string, string>::const_iterator it = fields.begin(); it != fields.end(); ++it)
	{
		string luafieldstr = "columns." + it->first + " = function() return " + it->second + " end " + it->first + " = columns." + it->first;
		int error = luaL_dostring(luaState, luafieldstr.c_str());
		if (error != LUA_OK) throw util::Exception(string("Lua Parse Error (") + it->first + "): " + lua_tostring(luaState, -1));
	}

	for (map<string, string>::const_iterator it = fields.begin(); it != fields.end(); ++it)
	{
		lua_getglobal(luaState, it->first.c_str());

		int error = lua_pcall(luaState, 0, 1, 0);
		string curr_result;
		if (error != LUA_OK)
		{
			curr_result = "-";
			//@TODO: Turn this on with some formula debugging option:
			//cerr << "lua err: " << lua_tostring(luaState, -1) << endl; // debug
		}
		else
		{
			const char * curr_result_c = lua_tostring(luaState, -1);
			curr_result = (curr_result_c != NULL) ? curr_result_c : "-";
			lua_pop(luaState, 1);
		}
		results[it->first] = curr_result;
	}

	lua_close(luaState);
	return results;
}

