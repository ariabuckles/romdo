OVERVIEW: Romdo (c) Jack Toole
  Romdo is a tool for manipulating text tables

USAGE: romdo <mainTable> <command> [parameters]

COMMANDS:
  export <saveToTable>
      Exports the values of all columns (without formulas) to <saveToTable>
  list
      Display all keys in <mainTable>
  merge [table1 ... [tableN]]
      Merge the tables in [table1..tableN] into <mainTable>
  show [key1 ... [keyN]]
      Display the record with each key in [key1..keyN] from <mainTable>
  update <key> <column> <newValue>
      Sets column <column> in record <key> to value <newValue>
  updateall <column> <newValue>
      Sets column <column> in all records to value <newValue>

OPTIONS:
  --help  Display this help

