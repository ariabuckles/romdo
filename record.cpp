#include "record.h"
#include "evaluate.h"

// The representation of 
const string Record::emptyValue = "-";

// Mutable access to a record's column
// Inserts the value emptyValue if absent
string & Record::operator[](const string & columnHeader)
{
	string & value = columnValues_[columnHeader];
	if (value == "")
		value = emptyValue;
	return value;
}

// Constant access to a record's column
// Returns a reference to emptyValue if absent
const string & Record::operator[](const string & columnHeader) const
{
	ColumnMap::const_iterator elem = columnValues_.find(columnHeader);
	if (elem == columnValues_.end() || elem->second == "")
		return emptyValue;
	else
		return elem->second;
}


void Record::erase(const string & columnHeader)
{
	columnValues_.erase(columnHeader);
}

// Test if two records are equal
bool Record::operator==(const Record & other) const
{
	return (key_ == other.key_) &&
		(columnValues_ == other.columnValues_);
}

// Merge the columns and values from a second record into this record.
// If a value is present in the second record, it overwrites the value in this
// record. For this purpose, any blank or emptyValue values count as that
// column being absent from a record.
void Record::mergeValuesFrom(const Record & otherRec)
{
	if (key_ == "")
		key_ = otherRec.key_;
	else if (key_ != otherRec.key_)
		throw util::Exception("Attempted to merge records " + key_ + " and " + otherRec.key_);

	for (ColumnMap::const_iterator it = otherRec.columnValues_.begin(); it != otherRec.columnValues_.end(); ++it)
	{
		string & ourValue = columnValues_[it->first];
		const string & otherValue = it->second;
		if (otherValue != Record::emptyValue && otherValue != "")
			ourValue = otherValue;
	}
}


Record Record::evaluate() const
{
	map<string, string> evaluated_map = ::evaluate(columnValues_);
	Record result;
	std::swap(result.columnValues_, evaluated_map);
	result.key_ = key_;
	return result;
}


void Record::print(ostream & out) const
{
	out << "Record: " << key_ << endl;
	
	// Print column headers and values line-by-line
	for (ColumnMap::const_iterator it = columnValues_.begin(); it != columnValues_.end(); ++it)
		out << it->first << ": " << it->second << endl;
}

