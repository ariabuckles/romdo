#include "romdo.h"

/**
 * Merges the tables in 'files' into this instance's main table
 */
void Romdo::merge(const vector<string> & files)
{
	vector<Table> tables(files.size());
	for (size_t i = 0; i < files.size(); i++)
		tables[i].readFromFile(files[i]);
	
	merge(tables);
}

/**
 * Merges the tables in 'tables' into this instance's main table
 */
void Romdo::merge(const vector<Table> & tables)
{
	for (size_t table_i = 0; table_i < tables.size(); table_i++)
		mainTable.mergeRecordsFrom(tables[table_i]);
}

void Romdo::showall(const string & column) const
{
	vector<string> keys = mainTable.keys();
	for (size_t i = 0; i < keys.size(); i++)
		show(keys[i], column);
}

/**
 * Display the record with key 'key'
 */
void Romdo::show(const string & key, const string & column /* = "" */) const
{
	const Record & rec = mainTable.get(key);
	Record evaluated_rec = rec.evaluate();
	if (column == "")
		evaluated_rec.print(cout);
	else
		cout << evaluated_rec.key() << Table::delimiter << evaluated_rec[column] << endl;
}

/**
 * Updates the record associated with the given key, updating its value for
 * the given column header.
 *
 * @param key Key to find our record with.
 * @param column Column header value to update.
 * @param value Value to update the column with.
 */
void Romdo::update(const string & key, const string & column, const string & value)
{
	mainTable.addHeader(column);
	Record & rec = mainTable.get(key);
	rec[column] = value;
}



void Romdo::updateall(const string & column, const string & value)
{
	vector<string> keys = mainTable.keys();
	for (size_t i = 0; i < keys.size(); i++)
		update(keys[i], column, value);
}


void Romdo::deletecolumn(const string & column)
{
	mainTable.removeHeader(column);
}


void Romdo::histogram(const string & column)
{
	vector<double> scores;
	vector<string> keys = mainTable.keys();
	scores.reserve(keys.size());
	for (size_t i = 0; i < keys.size(); i++)
	{
		string entry = mainTable.get(keys[i]).evaluate()[column];
		if (entry != Record::emptyValue)
		{
			stringstream ss(entry);
			double validEntry = 0.0;
			if ((ss >> validEntry) && validEntry >= 0.0)
				scores.push_back(validEntry);
		}
	}

	double maxScore = *std::max_element(scores.begin(), scores.end());
	vector<size_t> counts(std::max(0L, static_cast<ssize_t>(maxScore/10.0)) + 1);
	
	for (size_t i = 0; i < scores.size(); i++)
		counts[std::max(0L, static_cast<ssize_t>(scores[i]/10.0))]++;

	// create bucket headers
	vector<string> bucketHeaders;
	for (size_t row = 0; row < counts.size(); row++)
	{
		stringstream bucketHeaderSS;
		bucketHeaderSS << (row * 10) << '+';
		string nextBucketHeader;
		bucketHeaderSS >> nextBucketHeader;
		bucketHeaders.push_back(nextBucketHeader);
	}
	
	// Determin bucket heading size
	size_t maxBucketHeaderLength = 0;
	for (size_t row = 0; row < counts.size(); row++)
		maxBucketHeaderLength = std::max(maxBucketHeaderLength, bucketHeaders[row].length());

	// resize bucket headers to max size
	for (size_t row = 0; row < counts.size(); row++)
		bucketHeaders[row].resize(maxBucketHeaderLength, ' ');

	cout << column << " histogram:" << endl;
	cout << string(32, '=') << endl;
	for (size_t row = 0; row < counts.size(); row++)
		cout << bucketHeaders[row] << " | " << string(counts[row], 'x') << endl;
}


void Romdo::list(const vector<string> & params) const
{
	if (params.size() != 0)
		throw util::Exception("Too many arguments to 'list'");
	list();
}


void Romdo::list() const
{
	vector<string> keys = mainTable.keys();
	for (size_t i = 0; i < keys.size(); i++)
		cout << keys[i] << endl;
}

