#ifndef EVALUATE_H
#define EVALUATE_H

// Include Lua
extern "C" {
#include "liblua/lua.h"
#include "liblua/lualib.h"
#include "liblua/lauxlib.h"
}

#ifndef LUA_OK
#define LUA_OK 0
#endif

#include <map>
#include "util.h"

using namespace std;
using namespace util;

map<string, string> evaluate(const map<string, string> & fields);

#endif
