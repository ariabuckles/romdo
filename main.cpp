#include <string>
#include <vector>

/**
 * Call the appropriate, potentially application-specific, main function
 * This allows the other modules to be compiled into a testing environment
 */

namespace cs225
{
int main(const std::vector<std::string> & args);
}

int main(int argc, char ** argv)
{
	std::vector<std::string> args(argv, argv + argc);
	return cs225::main(args);
}

