#include <string>
#include <typeinfo>
#include <vector>

#include "romdo.h"
#include "util.h"

using namespace std;

// Print usage information and quit
void printUsageAndExit()
{
	cout << "USAGE: romdo <main.table> <command> [parameters]" << endl;
	cout << "For more help, run romdo --help" << endl;
	exit(1);
}

// Print help and quit
void printHelpAndExit()
{
	cout
#include "README_ROMDO.cgo"
	;
	exit(0);
}

// Romdo CLI pointer-to-member-function typedefs
typedef void (Romdo::*ConstRomdoCLIFunction)(const vector<string> &) const;
typedef void (Romdo::*ConstRomdoCLIFunction1)(const string &) const;
typedef void (Romdo::*ConstRomdoCLIFunction2)(const string &, const string &) const;
typedef void (Romdo::*ConstRomdoCLIFunction3)(const string &, const string &, const string &) const;
typedef void (Romdo::*MutableRomdoCLIFunction)(const vector<string> &);
typedef void (Romdo::*MutableRomdoCLIFunction1)(const string &);
typedef void (Romdo::*MutableRomdoCLIFunction2)(const string &, const string &);
typedef void (Romdo::*MutableRomdoCLIFunction3)(const string &, const string &, const string &);

/**
 * A partially curried call to a romdo function, leaving the functon name to be completed
 */
class RomdoCLIClosure {
	public:
	RomdoCLIClosure(Romdo & _romdo, const vector<string> & _params, const string & _mainTableFilename)
		: romdo(_romdo),
		  params(_params),
		  paramCount(static_cast<ssize_t>(params.size())),
		  mainTableFilename(_mainTableFilename) { }

	// Calls a Romdo const command without saving
	void call(ConstRomdoCLIFunction func)
	{
		(romdo.*func)(params);
	}

	void call(ConstRomdoCLIFunction1 func, ssize_t minParams = 1)
	{
		assertParams(minParams, 1);
		(romdo.*func)(params[0]);
	}

	void call(ConstRomdoCLIFunction2 func, ssize_t minParams = 2)
	{
		assertParams(minParams, 2);
		(romdo.*func)(params[0], params[1]);
	}

	void call(ConstRomdoCLIFunction3 func, ssize_t minParams = 3)
	{
		assertParams(minParams, 3);
		(romdo.*func)(params[0], params[1], params[2]);
	}


	// Calls a Romdo mutable command and saves
	void call(MutableRomdoCLIFunction func)
	{
		(romdo.*func)(params);
		romdo.save(mainTableFilename);
	}
	
	void call(MutableRomdoCLIFunction1 func, ssize_t minParams = 1)
	{
		assertParams(minParams, 1);
		(romdo.*func)(params[0]);
		romdo.save(mainTableFilename);
	}

	void call(MutableRomdoCLIFunction2 func, ssize_t minParams = 2)
	{
		assertParams(minParams, 2);
		(romdo.*func)(params[0], params[1]);
		romdo.save(mainTableFilename);
	}

	void call(MutableRomdoCLIFunction3 func, ssize_t minParams = 3)
	{
		assertParams(minParams, 3);
		(romdo.*func)(params[0], params[1], params[2]);
		romdo.save(mainTableFilename);
	}

	private:
	Romdo & romdo;
	vector<string> params;
	const ssize_t paramCount;
	const string & mainTableFilename;
	
	void assertParams(ssize_t minCount, ssize_t maxCount = -1)
	{
		if (maxCount == -1) maxCount = minCount;
		if (paramCount < minCount)
			throw util::Exception(string("Too few arguments: found ") + paramCount + ", expected " + minCount);
		if (paramCount > maxCount)
			throw util::Exception(string("Too many arguments: found ") + paramCount + ", expected " + minCount);
		params.resize(maxCount); // add ""s as the end parameters if necessary
	}
};


/**
 * Designate this as the cs225::main to be called by the global main
 * Keeping this namespaced means this module could be easily compiled
 * into a test
 */
namespace cs225
{

/**
 * The Romdo CLI
 */
int main(const std::vector<std::string> & args)
{
	// Parse command line arguments
	OptionsParser options;
	
	// Options
	bool opts_help = false;
	options.addOption("help", opts_help);
	options.addOption("h",    opts_help);

	// Arguments
	string mainTableFile = "";
	string command = "";
	options.addArg(mainTableFile);
	options.addArg(command);
	vector<string> params = options.parse(args);

	if (opts_help)
		printHelpAndExit();

	if (mainTableFile == "" || command == "")
		printUsageAndExit();

	try
	{
		Romdo instance(mainTableFile);
		RomdoCLIClosure closure(instance, params, mainTableFile);

		// Switch on the command
		makeLower(command);
		if      (command == "merge")        closure.call(&Romdo::merge);
		else if (command == "show" )        closure.call(&Romdo::show, 1);
		else if (command == "showall" )     closure.call(&Romdo::showall);
		else if (command == "update")       closure.call(&Romdo::update);
		else if (command == "updateall")    closure.call(&Romdo::updateall);
		else if (command == "deletecolumn") closure.call(&Romdo::deletecolumn);
		else if (command == "histogram")    closure.call(&Romdo::histogram);
		else if (command == "list")         closure.call(&Romdo::list);
		else if (command == "export")       closure.call(&Romdo::exportto);
		else printUsageAndExit();
		
		return 0;
	}
	catch (const exception & e)
	{
		// Report any error encountered during execution
		cerr << "Error: " << e.what() << endl;
		return 1;
	}
}

} // namespace cs225

